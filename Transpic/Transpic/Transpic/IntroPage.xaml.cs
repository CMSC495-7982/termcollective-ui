﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

/*+----------------------------------------------------------------------
 ||
 ||  Class IntroPage.xaml.cs 
 ||
 ||         Author:  Sukhmeet Khalar
 ||                  Kamilah Carlisle
 ||                  Ben Broadstone
 ||                  Alexander Vernet
 ||                  Kurtis Crowe
 ||                  Andrew Renteria
 ||
 ||        Purpose:  Introduction page for the application. This will only appear once when the application
 ||                  is first launched. It will not appear again unless the app has been reinstalled
 ||
 ||
 ++-----------------------------------------------------------------------*/

namespace Transpic
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IntroPage : ContentPage
    {
        public IntroPage()
        {
            Preferences.Set("FirstLaunch", false);
            InitializeComponent();
        }
        
        /*
         * overides the base OnAppearing() method to allow for
         * intro page to stay alive for 6 seconds (6000ms)
         * before navigating the user to the Main Page
         */
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await Task.Delay(6000);
            await this.Navigation.PushAsync(new MainPage());
        }
    }
}